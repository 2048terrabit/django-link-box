from django.apps import AppConfig


class LiboxConfig(AppConfig):
    name = 'libox'
