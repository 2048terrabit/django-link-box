from django.conf.urls import url
from . import views

app_name = 'libox'

urlpatterns = [
    url(r'^$', views.index, name='index'),

    # /links/552/
    # url(r'^(?P<card_id>[0-9]+)/$', views.detail, name='detail'),

    # register
    url(r'^register/$', views.UserFormView.as_view(), name='register'),

    # login
    url(r'^login/$', views.UserLoginView.as_view(), name='login'),

    # logout
    url(r'^logout/$', views.logout_view, name='logout'),

    # LinkCard api
    url(r'^api/linkcard/$', views.LinkCardApi.as_view(), name='linkcard_api'),
]