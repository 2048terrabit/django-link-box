from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class LinkCard(models.Model):
    link = models.CharField(max_length=1024)
    comment = models.CharField(max_length=1024)
    createdAt = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=None, null=True, blank=True)
    isFavorite = models.BooleanField(default=False)

    def __str__(self):
        return self.comment[:25] + ' - ' + self.user.username

    def as_dict(self):
        return {
            'link'      : self.link,
            'comment'   : self.comment,
            'createdAt' : str(self.createdAt),
            'id'        : self.pk,
            'isFav'     : self.isFavorite
        }

    class Meta:
        ordering = ['createdAt']
