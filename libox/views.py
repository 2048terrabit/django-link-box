from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login, logout
from django.views.generic import View
from .forms import UserForm
from .models import LinkCard
from django.views.generic.edit import DeleteView
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponse

from django.views.decorators.csrf import csrf_protect

from django.conf import settings

import json

# Create your views here.
def index(request):
    if request.user.is_authenticated():
        all_links = LinkCard.objects.filter(user=request.user).all()[::-1]
        context = {'all_links': all_links[:10]}
        return render(request, 'libox/index.html', context)
    else:
        return redirect('libox:login')

def detail(request, card_id):
    linkcard = get_object_or_404(LinkCard, id=card_id)
    
    return render(request, 'libox/detail.html', {'linkcard': linkcard})


class LinkCardApi(View):
    
    response = {}

    def get(self, request):
        if request.user.is_authenticated():
            offset = int( request.GET.get('offset', 0) )
            all_links = LinkCard.objects.filter(user=request.user).all()[::-1]
            dict_of_links = [link.as_dict() for link in all_links[offset : offset + 10]]
            self.response = {"list": dict_of_links, "total": len(all_links) }
        else:
            self.response = {"error":"No auth"}

        return HttpResponse(json.dumps(self.response), content_type="application/json")

    def post(self, request):
        if request.user.is_authenticated():
            try:
                # request.POST.get doesn't work with js fetch polyfill O_o
                json_data = json.loads( request.body.decode('utf8') )
                link    = json_data['link']
                comment = json_data['comment']
                user    = request.user.id

                card = LinkCard(link=link, comment=comment, user_id=user)
                card.save()

                self.response = {"success":True, "data":card.as_dict()}
            except:
                self.response = {"success":False, "error":"Unknown"}
        else:
            self.response = {"success":False, "error":"No auth"}
            return redirect('libox:login')

        return HttpResponse(json.dumps(self.response), content_type="application/json")

    def patch(self, request):
        if request.user.is_authenticated():
            try:
                json_data = json.loads( request.body.decode('utf8') )

                pk    = json_data['id']
                isFav = json_data['isFav']
                user  = request.user.id


                card = LinkCard.objects.filter(pk=pk, user_id=request.user.id)
                card.update(isFavorite=isFav)
                card = card.get()

                self.response = {"success":True, "data":card.as_dict()}
            except Exception as e:
                if settings.DEBUG:
                    self.response = {"success":False, "error":str(e)}
                else:
                    self.response = {"success":False, "error":"Unknown"}
        else:
            self.response = {"success":False, "error":"No auth"}
        return HttpResponse(json.dumps(self.response), content_type="application/json")

    def delete(self, request):
        if request.user.is_authenticated():
            try:
                # Parsing request's body as json
                json_data = json.loads( request.body.decode('utf8') )

                pk   = json_data['id']
                user = request.user.id

                card = LinkCard.objects.get(pk=pk, user_id=request.user.id)
                card.delete()

                self.response = {"success":True}
            except:
                self.response = {"success":False, "error":"No such card"}
        else:
            self.response = {"success":False, "error":"No auth"}

        return HttpResponse(json.dumps(self.response), content_type="application/json")



class UserFormView(View):
    form_class = UserForm
    template_name = 'libox/registration_form.html'

    # display blank form
    def get(self, request):
        form = self.form_class(None)
        return render(request, self.template_name, {'form':form})

    # process form data
    def post(self, request):
        form = self.form_class(request.POST)

        if form.is_valid():
            user = form.save(commit=False)

            # cleaned (normalized) data
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user.set_password(password)
            user.save()

            # returns User objects if credentials are correct
            user = authenticate(username=username, password=password)

            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect('libox:index')

        return render(request, self.template_name, {'form':form})



class UserLoginView(View):
    form = ['username','password']
    template_name = 'libox/login.html'

    def get(self, request):
        return render(request, self.template_name, {"form":self.form,'error':''})
        
    def post(self, request):

        # cleaned (normalized) data
        username = request.POST.get("username", "")
        password = request.POST.get("password", "")


        # returns User objects if credentials are correct
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect('libox:index')

        return render(request, self.template_name, {'form':self.form,'error':'There was an error.'})

def my_view(request):
    username = request.POST.get('username','')
    password = request.POST.get('password','')
    user = authenticate(username=username, password=password)
    if user is not None:
        login(request, user)
        return redirect('libox:index')
    return render(request, 'libox/login.html')

def logout_view(request):
    logout(request)
    return redirect('libox:login')